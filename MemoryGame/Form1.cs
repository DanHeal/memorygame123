﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MemoryGame
{
    public partial class Form1 : Form
    {


        bool easy = false, medium = false, hard = false;
        Random rnd = new Random();
        PictureBox[] allp;
        int count = 0;
        PictureBox p1, p2;
        int countTotal1 = 0, countTotal2 = 0, countT = 0;
        int tcount = 1;
        bool player1 = true, player2 = false;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
            lblFirstName.Visible = false;
            lblSecondName.Visible = false;
            lblP1.Visible = false;
            lblP2.Visible = false;
            lblTurn.Visible = false;
            lblWinnerName.Visible = false;
            lblWin.Visible = false;

        }
        private void shuffle()
        {
            int num;
            PictureBox temp;
            for (int i = 0; i < allp.Length; i++)
            {
                num = rnd.Next(0, allp.Length);
                temp = allp[i];
                allp[i] = allp[num];
                allp[num] = temp;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (rdEasy.Checked)
                easy = true;
            else if (rdMedium.Checked)
                medium = true;
            else
                hard = true;
            



            lblFirstName.Text = txtP1.Text + ":";
            lblSecondName.Text = txtP2.Text + ":";
            lblTurn.Text = txtP1.Text + "";
            

            rdEasy.Visible = false;
            rdMedium.Visible = false;
            rdHard.Visible = false;
            lblLevel.Visible = false;
            txtP1.Visible = false;
            txtP2.Visible = false;
            lblP1Name.Visible = false;
            lblP2Name.Visible = false;
            btnStart.Visible = false;

            if (hard)
            {
                buildPicture(40);
                shuffle();
                showBoard(5, 8);
            }
            else if (medium)
            {
                buildPicture(24);
                shuffle();
                showBoard(4,6);
            }
            else
            {
                buildPicture(16);
                shuffle();
                showBoard(4,4);
            }

            lblP1.Visible = true;
            lblP2.Visible = true;
            lblFirstName.Visible = true;
            lblSecondName.Visible = true;
            lblTurn.Visible = true;
            lblWinnerName.Visible = true;
            lblWin.Visible = true;
        }

        
        private void showBoard(int row, int col)
        {
            int x = 100, y = 100;
            int place = 0;
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    allp[place].Image= (Image)Properties.Resources.ResourceManager.GetObject("pic0");
                    allp[place].Location = new Point(x, y);
                    x += 90;
                    Controls.Add(allp[place++]);
                }
                y += 95;
                x = 100;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2();
            f2.ShowDialog();
            this.Close();
        }

        private void buildPicture(int size)
        {
            int picNum = 1;
            allp = new PictureBox[size];
            for (int i = 0; i < allp.Length - 1; i += 2)
            {
                allp[i] = new PictureBox();
                allp[i].Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (picNum));
                allp[i].Size = new Size(90, 90);
                allp[i].SizeMode = PictureBoxSizeMode.StretchImage;
                allp[i].BorderStyle = BorderStyle.FixedSingle;
                allp[i].Tag = picNum + "";
                allp[i].Click += allp_click;

                allp[i + 1] = new PictureBox();
                allp[i + 1].Tag = picNum + "";
                allp[i + 1].Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (picNum));
                picNum++;
                allp[i + 1].Size = new Size(90, 90);
                allp[i + 1].SizeMode = PictureBoxSizeMode.StretchImage;
                allp[i + 1].BorderStyle = BorderStyle.FixedSingle;
                allp[i + 1].Click += allp_click;
            }
        }

        private void allp_click(object sender, EventArgs e)
        {

            if (count == 0)
            {
                p1 = (PictureBox)sender;
                p1.Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (p1.Tag));
                count++;
            }
            else if (count == 1)
            {
                p2 = (PictureBox)sender;
                p2.Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (p2.Tag));
                if (p1.Tag.Equals(p2.Tag))
                {
                    countT++;
                    if (easy && countT == 8)
                        Check();
                    else if (medium && countT == 12)
                        Check();
                    else if (hard && countT == 20)
                        Check();
                    else
                    {
                        if (player1)
                        {
                            countTotal1++;
                            lblP1.Text = countTotal1 + "";
                            player1 = false;
                            player2 = true;
                            lblTurn.Text = lblSecondName.Text;
                        }
                        else
                        {
                            countTotal2++;
                            lblP2.Text = countTotal2 + "";
                            player1 = true;
                            player2 = false;
                            lblTurn.Text = lblFirstName.Text;
                        }

                        timerpic.Enabled = true;


                    }
                }
                else
                {
                    if (player1)
                    {
                        player1 = false;
                        player2 = true;
                        lblTurn.Text = lblSecondName.Text;
                    }
                    else 
                    {
                        player1 = true;
                        player2 = false;
                        lblTurn.Text = lblFirstName.Text;
                    }
                    timerpic.Enabled = true;
                   
                }
                count = 0;
            }
               
        }


        public void Check()
        {
            if (countTotal1 > countTotal2)
                lblWin.Text = lblFirstName.Text;
            else if (countTotal2 > countTotal1)
                lblWin.Text = lblSecondName.Text;
            else
                lblWin.Text = "no one";
            timerpic.Enabled = true;
            lblFirstName.Visible = false;
            lblSecondName.Visible = false;
            lblTurn.Visible = false;



        }
        private void timerpic_Tick(object sender, EventArgs e)
        {
            
            tcount--;
            if (tcount == 0)
            {
                timerpic.Enabled = false;
                if (p1.Tag.Equals(p2.Tag))
                {
                    p1.Visible = false;
                    p2.Visible = false;
                }
                else
                {
                    p1.Image = (Image)Properties.Resources.ResourceManager.GetObject("pic0");
                    p2.Image = (Image)Properties.Resources.ResourceManager.GetObject("pic0");
                }
                
                tcount = 1;
            }
        }
    }
}

