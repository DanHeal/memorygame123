﻿namespace MemoryGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerpic = new System.Windows.Forms.Timer(this.components);
            this.lblP2 = new System.Windows.Forms.Label();
            this.lblP1 = new System.Windows.Forms.Label();
            this.txtP2 = new System.Windows.Forms.TextBox();
            this.txtP1 = new System.Windows.Forms.TextBox();
            this.lblP1Name = new System.Windows.Forms.Label();
            this.lblP2Name = new System.Windows.Forms.Label();
            this.rdEasy = new System.Windows.Forms.RadioButton();
            this.rdHard = new System.Windows.Forms.RadioButton();
            this.rdMedium = new System.Windows.Forms.RadioButton();
            this.lblLevel = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblSecondName = new System.Windows.Forms.Label();
            this.lblWin = new System.Windows.Forms.Label();
            this.lblWinnerName = new System.Windows.Forms.Label();
            this.lblTurn = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timerpic
            // 
            this.timerpic.Interval = 500;
            this.timerpic.Tick += new System.EventHandler(this.timerpic_Tick);
            // 
            // lblP2
            // 
            this.lblP2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP2.Location = new System.Drawing.Point(1263, 270);
            this.lblP2.Name = "lblP2";
            this.lblP2.Size = new System.Drawing.Size(100, 36);
            this.lblP2.TabIndex = 0;
            // 
            // lblP1
            // 
            this.lblP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP1.Location = new System.Drawing.Point(1007, 270);
            this.lblP1.Name = "lblP1";
            this.lblP1.Size = new System.Drawing.Size(100, 36);
            this.lblP1.TabIndex = 1;
            // 
            // txtP2
            // 
            this.txtP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtP2.Location = new System.Drawing.Point(352, 167);
            this.txtP2.Name = "txtP2";
            this.txtP2.Size = new System.Drawing.Size(131, 29);
            this.txtP2.TabIndex = 2;
            // 
            // txtP1
            // 
            this.txtP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtP1.Location = new System.Drawing.Point(352, 48);
            this.txtP1.Name = "txtP1";
            this.txtP1.Size = new System.Drawing.Size(131, 29);
            this.txtP1.TabIndex = 3;
            // 
            // lblP1Name
            // 
            this.lblP1Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP1Name.Location = new System.Drawing.Point(12, 19);
            this.lblP1Name.Name = "lblP1Name";
            this.lblP1Name.Size = new System.Drawing.Size(334, 49);
            this.lblP1Name.TabIndex = 4;
            this.lblP1Name.Text = "insert the name of the first player:";
            // 
            // lblP2Name
            // 
            this.lblP2Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP2Name.Location = new System.Drawing.Point(12, 116);
            this.lblP2Name.Name = "lblP2Name";
            this.lblP2Name.Size = new System.Drawing.Size(317, 57);
            this.lblP2Name.TabIndex = 5;
            this.lblP2Name.Text = "insert the name of the second player:";
            // 
            // rdEasy
            // 
            this.rdEasy.AutoSize = true;
            this.rdEasy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdEasy.Location = new System.Drawing.Point(709, 144);
            this.rdEasy.Name = "rdEasy";
            this.rdEasy.Size = new System.Drawing.Size(78, 29);
            this.rdEasy.TabIndex = 7;
            this.rdEasy.TabStop = true;
            this.rdEasy.Text = "Easy";
            this.rdEasy.UseVisualStyleBackColor = true;
            // 
            // rdHard
            // 
            this.rdHard.AutoSize = true;
            this.rdHard.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdHard.Location = new System.Drawing.Point(709, 309);
            this.rdHard.Name = "rdHard";
            this.rdHard.Size = new System.Drawing.Size(76, 29);
            this.rdHard.TabIndex = 8;
            this.rdHard.TabStop = true;
            this.rdHard.Text = "Hard";
            this.rdHard.UseVisualStyleBackColor = true;
            // 
            // rdMedium
            // 
            this.rdMedium.AutoSize = true;
            this.rdMedium.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdMedium.Location = new System.Drawing.Point(709, 232);
            this.rdMedium.Name = "rdMedium";
            this.rdMedium.Size = new System.Drawing.Size(106, 29);
            this.rdMedium.TabIndex = 9;
            this.rdMedium.TabStop = true;
            this.rdMedium.Text = "Medium";
            this.rdMedium.UseVisualStyleBackColor = true;
            // 
            // lblLevel
            // 
            this.lblLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(704, 65);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(210, 36);
            this.lblLevel.TabIndex = 10;
            this.lblLevel.Text = "insert the difficulty:";
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(85, 357);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(230, 148);
            this.btnStart.TabIndex = 11;
            this.btnStart.Text = "start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblFirstName
            // 
            this.lblFirstName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstName.Location = new System.Drawing.Point(901, 270);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(100, 36);
            this.lblFirstName.TabIndex = 12;
            // 
            // lblSecondName
            // 
            this.lblSecondName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSecondName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecondName.Location = new System.Drawing.Point(1157, 270);
            this.lblSecondName.Name = "lblSecondName";
            this.lblSecondName.Size = new System.Drawing.Size(100, 36);
            this.lblSecondName.TabIndex = 13;
            // 
            // lblWin
            // 
            this.lblWin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblWin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWin.Location = new System.Drawing.Point(1050, 482);
            this.lblWin.Name = "lblWin";
            this.lblWin.Size = new System.Drawing.Size(148, 37);
            this.lblWin.TabIndex = 14;
            // 
            // lblWinnerName
            // 
            this.lblWinnerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWinnerName.Location = new System.Drawing.Point(884, 482);
            this.lblWinnerName.Name = "lblWinnerName";
            this.lblWinnerName.Size = new System.Drawing.Size(150, 23);
            this.lblWinnerName.TabIndex = 15;
            this.lblWinnerName.Text = "The winner is:";
            // 
            // lblTurn
            // 
            this.lblTurn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurn.Location = new System.Drawing.Point(1073, 193);
            this.lblTurn.Name = "lblTurn";
            this.lblTurn.Size = new System.Drawing.Size(100, 45);
            this.lblTurn.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(1375, 647);
            this.Controls.Add(this.lblTurn);
            this.Controls.Add(this.lblWinnerName);
            this.Controls.Add(this.lblWin);
            this.Controls.Add(this.lblSecondName);
            this.Controls.Add(this.lblFirstName);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lblLevel);
            this.Controls.Add(this.rdMedium);
            this.Controls.Add(this.rdHard);
            this.Controls.Add(this.rdEasy);
            this.Controls.Add(this.lblP2Name);
            this.Controls.Add(this.lblP1Name);
            this.Controls.Add(this.txtP1);
            this.Controls.Add(this.txtP2);
            this.Controls.Add(this.lblP1);
            this.Controls.Add(this.lblP2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerpic;
        private System.Windows.Forms.Label lblP2;
        private System.Windows.Forms.Label lblP1;
        private System.Windows.Forms.TextBox txtP2;
        private System.Windows.Forms.TextBox txtP1;
        private System.Windows.Forms.Label lblP1Name;
        private System.Windows.Forms.Label lblP2Name;
        private System.Windows.Forms.RadioButton rdEasy;
        private System.Windows.Forms.RadioButton rdHard;
        private System.Windows.Forms.RadioButton rdMedium;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblSecondName;
        private System.Windows.Forms.Label lblWin;
        private System.Windows.Forms.Label lblWinnerName;
        private System.Windows.Forms.Label lblTurn;
    }
}

